<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Provider;
use App\Models\Service;

class LaboratoryController extends Controller
{
    public function index()
    {
        $providers  = Provider::where('type','laboratory')->get();
        return view('admin.laboratories.index')->with('providers',$providers);
    }
    public function create()
    {
        return view('admin.laboratories.create');
    }
    public function store(Request $request)
    {
        Provider::create([
            'name_en' => $request->name_en,
            'name_ar' => $request->name_ar,
            'type' => 'laboratory',
        ]);

        return redirect()->route('admin.laboratories.index');
    }
    public function show($id)
    {
        $services = Service::where('type','analyze')->get();
        $provider = Provider::find($id);
        return view('admin.laboratories.show')->with('provider',$provider)->with('services',$services);
    }

    public function assign(Request $request,$id)
    {
        $services = Service::where('type','analyze')->get();
        $service =  Service::find($request->service_id);
        $provider = Provider::find($id);
        $provider->services()->attach($request->service_id,['original_price'=>$request->original_price,'sell_price'=>$request->sell_price,'purchase_price'=>$request->sell_price]);
        return redirect()->route('admin.laboratories.show',$provider)->with('provider',$provider)->with('services',$services);
    }

    public function unassign(Request $request,$id)
    {
        $services = Service::where('type','analyze')->get();
        $service =  Service::find($request->service_id);
        $provider = Provider::find($id);
        $provider->services()->detach($request->service_id);
        return redirect()->route('admin.laboratories.show',$provider)->with('provider',$provider)->with('services',$services);
    }

}
