<?php

namespace App\Http\Controllers;


use App\Imports\XrayImport;
use App\Models\Service;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class XrayController extends Controller
{
    public function index()
    {
        $services  = Service::where('type','xray')->get();
        return view('admin.xrays.index')->with('services',$services);
    }
    public function create()
    {
        return view('admin.xrays.create');
    }
    public function store(Request $request)
    {
        Service::create([
            'name_en' => $request->name_en,
            'name_ar' => $request->name_ar,
            'type' => 'xray',
        ]);

        return redirect()->route('admin.xrays.index');
    }

    public function fileImport(Request $request) 
    {
        Excel::import(new XrayImport, $request->file('file')->store('temp'));
        return back();
    }

}
