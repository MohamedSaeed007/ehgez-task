<?php

namespace App\Http\Controllers;

use App\Models\Provider;
use App\Models\Service;
use Illuminate\Http\Request;

class HospitalController extends Controller
{
    public function index()
    {
        $providers  = Provider::where('type','hospital')->get();
        return view('admin.hospitals.index')->with('providers',$providers);
    }
    public function create()
    {
        return view('admin.hospitals.create');
    }
    public function store(Request $request)
    {
        Provider::create([
            'name_en' => $request->name_en,
            'name_ar' => $request->name_ar,
            'type' => 'hospital',
        ]);

        return redirect()->route('admin.hospitals.index');
    }
    public function show($id)
    {
        $services = Service::where('type','surgery')->get();
        $provider = Provider::find($id);
        return view('admin.hospitals.show')->with('provider',$provider)->with('services',$services);
    }

    public function assign(Request $request,$id)
    {
        $services = Service::where('type','surgery')->get();
        $service =  Service::find($request->service_id);
        $provider = Provider::find($id);
        $provider->services()->attach($request->service_id,['original_price'=>$request->original_price,'sell_price'=>$request->sell_price,'purchase_price'=>$request->sell_price]);
        return redirect()->route('admin.hospitals.show',$provider)->with('provider',$provider)->with('services',$services);
    }

    public function unassign(Request $request,$id)
    {
        $services = Service::where('type','surgery')->get();
        $service =  Service::find($request->service_id);
        $provider = Provider::find($id);
        $provider->services()->detach($request->service_id);
        return redirect()->route('admin.hospitals.show',$provider)->with('provider',$provider)->with('services',$services);
    }
}
