<?php

namespace App\Http\Controllers;

use App\Imports\SurgeryImport;
use App\Models\Service;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class SurgeryController extends Controller
{
    public function index()
    {
        $services  = Service::where('type','surgery')->get();
        return view('admin.surgeries.index')->with('services',$services);
    }
    public function create()
    {
        return view('admin.surgeries.create');
    }
    public function store(Request $request)
    {
        Service::create([
            'name_en' => $request->name_en,
            'name_ar' => $request->name_ar,
            'type' => 'surgery',
        ]);

        return redirect()->route('admin.surgeries.index');
    }

    public function fileImport(Request $request) 
    {
        Excel::import(new SurgeryImport, $request->file('file')->store('temp'));
        return back();
    }

}
