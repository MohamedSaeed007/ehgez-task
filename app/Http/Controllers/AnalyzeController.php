<?php

namespace App\Http\Controllers;

use App\Imports\AnalyzesImport;
use App\Models\Service;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class AnalyzeController extends Controller
{
    public function index()
    {
        $services  = Service::where('type','analyze')->get();
        return view('admin.analyzes.index')->with('services',$services);
    }
    public function create()
    {
        return view('admin.analyzes.create');
    }
    public function store(Request $request)
    {
        Service::create([
            'name_en' => $request->name_en,
            'name_ar' => $request->name_ar,
            'type' => 'analyze',
        ]);

        return redirect()->route('admin.analyzes.index');
    }

    public function fileImport(Request $request) 
    {
        Excel::import(new AnalyzesImport, $request->file('file')->store('temp'));
        return back();
    }

}
