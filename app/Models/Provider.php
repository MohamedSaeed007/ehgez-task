<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    use HasFactory;
    protected $fillable = [
        'name_ar','name_en','type'
    ];

    public function services(){
       return $this->belongsToMany(Service::class)->withTimestamps();
    }
}
