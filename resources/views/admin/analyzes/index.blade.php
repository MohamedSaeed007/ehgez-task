@extends('admin.layouts.master')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Analyzes List</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Analyzes List</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Analyzes List</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div style="margin-bottom: 10px; float: right;">
                                    <a href="{{ route('admin.analyzes.create') }}" class="btn btn-primary">Add Analyze</a>
                                    <form style="display: inline" action="{{ route('admin.analyzes.file-import') }}" method="POST"
                                        enctype="multipart/form-data">
                                        @csrf
                                        <input id="file" type="file" name="file" style="display: none" onchange="this.form.submit();">
                                        <button type="button" class="btn btn-success" onclick="thisFileUpload();">Import From Excel</button>
                                    </form>
                                </div>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th style="width: 10px">#</th>
                                            <th>Name</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($services as $index => $service)
                                            <tr>
                                                <td>{{ $index + 1 }}</td>
                                                <td>
                                                    @if (LaravelLocalization::getCurrentLocale() == 'ar')
                                                        {{ $service->name_ar }}
                                                    @else
                                                        {{ $service->name_en }}
                                                    @endif
                                                </td>
                                              
                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->

                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@section('scripts')
<script>
    function thisFileUpload() {
        document.getElementById("file").click();
    };
</script>
@endsection
