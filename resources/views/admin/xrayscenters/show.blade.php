@extends('admin.layouts.master')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Assign Analyze to {{ $provider->name_en }}</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Assign Analyze</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Analyzes List</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">

                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th style="width: 10px">#</th>
                                            <th>Name</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($services as $index => $service)
                                            <tr>
                                                <td>{{ $index + 1 }}</td>
                                                <td>
                                                    @if (LaravelLocalization::getCurrentLocale() == 'ar')
                                                        {{ $service->name_ar }}
                                                    @else
                                                        {{ $service->name_en }}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($provider->services()->where('service_id', $service->id)->first())
                                                        <form action="{{ route('admin.xrayscenters.unassign',$provider) }}" method="POST">
                                                            @csrf
                                                            <input type="hidden" value="{{ $service->id }}" name="service_id">
                                                            <button type="submit" class="btn btn-danger">
                                                                Unassign
                                                            </button>
                                                        </form>
                                                    @else
                                                        <button  type="button"
                                                            data-id="{{ $service->id }}" class="assign_button btn btn-primary"
                                                            data-toggle="modal" data-target="#modal-default">
                                                            Assign
                                                        </button>
                                                    @endif


                                                </td>
                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                            <div class="modal fade" id="modal-default">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Default Modal</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form action="{{ route('admin.xrayscenters.assign', $provider->id) }}"
                                            method="POST">
                                            @csrf
                                            <div class="modal-body">
                                                <input type="hidden" id="service_id" name="service_id">
                                                <div class="form-group">
                                                    <label for="original_price">Original Price</label>
                                                    <input type="number" class="form-control" name="original_price"
                                                        placeholder="Enter Original Price">
                                                </div>
                                                <div class="form-group">
                                                    <label for="sell_price">Sell Price</label>
                                                    <input type="number" class="form-control" name="sell_price"
                                                        placeholder="Enter Sell Price">
                                                </div>
                                                <div class="form-group">
                                                    <label for="purchase_price">Purchase Price</label>
                                                    <input type="number" class="form-control" name="purchase_price"
                                                        placeholder="Enter Purchase Price">
                                                </div>
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                                <button type="button" class="btn btn-default"
                                                    data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Save changes</button>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('scripts')
    <script>
        $('.assign_button').on('click', function() {
            const button = $(this)
            $('#service_id').val(button.data('id'));
        });
    </script>
@endsection
