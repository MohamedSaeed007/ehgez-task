<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AnalyzeController;
use App\Http\Controllers\HospitalController;
use App\Http\Controllers\LaboratoryController;
use App\Http\Controllers\SurgeryController;
use App\Http\Controllers\XrayCenterController;
use App\Http\Controllers\XrayController;

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale().'/admin',
        'as'=> 'admin.',
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    ], function(){ 
        Route::get('/dashboard', function () {
            return view('admin.dashboard');
        });
        
        Route::resource('/analyzes', AnalyzeController::class);
        Route::resource('/laboratories', LaboratoryController::class);
        Route::post('/laboratories/assign/{laboratory}', [LaboratoryController::class,'assign'])->name('laboratories.assign');
        Route::post('/laboratories/unassign/{laboratory}', [LaboratoryController::class,'unassign'])->name('laboratories.unassign');
        Route::post('/analyzes/file-import', [AnalyzeController::class, 'fileImport'])->name('analyzes.file-import');

        Route::resource('/xrays', XrayController::class);
        Route::resource('/xrayscenters', XrayCenterController::class);
        Route::post('/xrayscenters/assign/{xraycenter}', [XrayCenterController::class,'assign'])->name('xrayscenters.assign');
        Route::post('/xrayscenters/unassign/{xraycenter}', [XrayCenterController::class,'unassign'])->name('xrayscenters.unassign');
        Route::post('/xrays/file-import', [XrayController::class, 'fileImport'])->name('xrays.file-import');

        Route::resource('/surgeries', SurgeryController::class);
        Route::resource('/hospitals', HospitalController::class);
        Route::post('/hospitals/assign/{hospital}', [HospitalController::class,'assign'])->name('hospitals.assign');
        Route::post('/hospitals/unassign/{hospital}', [HospitalController::class,'unassign'])->name('hospitals.unassign');
        Route::post('/surgeries/file-import', [SurgeryController::class, 'fileImport'])->name('surgeries.file-import');
    });
